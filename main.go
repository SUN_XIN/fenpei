package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"github.com/pkg/errors"
)

var (
	outputMsg  []string
	productMap map[int][]Show

	minProductsPerStreaming int // 每场直播最少播的产品数量
	maxProductsPerStreaming int // 每场直播最多播的产品数量
	maxStreamingPerProduct  int // 每件产品最多被播的次数

	homeBlockTime int // 在家直播的产品，N天之后才能分配给其他主播
)

const (
	fenpeiDatefFormat = "02/01/2006"
	fenpeiBucket      = "ceshi_upload"
	// OUTPUT_FILE_BLOGGER_PATH is the output file that contains the result that is ordered by blogger.
	OUTPUT_FILE_BLOGGER_PATH = "fenpei/res_by_blogger"
	// OUTPUT_FILE_TIMING_PATH is the output file that contains the result that is ordered by timing.
	OUTPUT_FILE_TIMING_PATH = "fenpei/res_by_timing"
	// OUTPUT_FILE_PLACE_PATH is the output file that contains the result that is ordered by place.
	OUTPUT_FILE_PLACE_PATH = "fenpei/res_by_place"

	PlaceHome = "Home"

	// BLOGGER_INFO_INDEX_NAME is the index about blogger name in the file.
	// 在博主信息的excel文件里面，关于博主名字那一竖行的编号（从0开始）,就是第几竖行
	BLOGGER_INFO_INDEX_NAME         = 0
	BLOGGER_INFO_INDEX_SOURCE       = 1
	BLOGGER_INFO_INDEX_NICKNAME     = 2
	BLOGGER_INFO_INDEX_LINK         = 3
	BLOGGER_INFO_INDEX_NUM_FANS     = 4
	BLOGGER_INFO_INDEX_ACCOUNT_NAME = 5
	BLOGGER_INFO_INDEX_SEX          = 6
	BLOGGER_INFO_INDEX_DOMAINS      = 7
	BLOGGER_INFO_INDEX_RECIPIENT    = 8
	BLOGGER_INFO_INDEX_ADDR         = 9
	BLOGGER_INFO_INDEX_ZIP          = 10
	BLOGGER_INFO_INDEX_CITY         = 11
	BLOGGER_INFO_INDEX_TELE         = 12

	// PRODUCTS_INFO_INDEX_NAME is the index about product name in the file.
	// 在产品信息的excel文件里面，关于产品名字那一竖行的编号（从0开始）,就是第几竖行
	PRODUCTS_INFO_INDEX_NAME    = 2
	PRODUCTS_INFO_INDEX_ID      = 3
	PRODUCTS_INFO_INDEX_LINK    = 4
	PRODUCTS_INFO_INDEX_STATUS  = 5
	PRODUCTS_INFO_INDEX_DOMAINS = 8

	// TIMING_INDEX_DATE is the index about show date.
	// 在场地信息的excel文件里面，关于日期那一竖行的编号（从0开始）,就是第几竖行
	TIMING_INDEX_DATE         = 0
	TIMING_INDEX_BLOGGER_NAME = 1
	TIMING_INDEX_START        = 4
	TIMING_INDEX_END          = 6
	TIMING_INDEX_PLACE        = 7
	TIMING_INDEX_ROOM_ID      = 12
)

func main() {
	// fenpei
	http.HandleFunc("/fenpei/index", FenpeiIndexServer)
	http.HandleFunc("/fenpei/process", FenpeiServer)
	http.HandleFunc("/fenpei/table", FenpeiTableServer)

	port := "8080"
	log.Printf("Listening on port %s", port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
	}
}

func fenpeiWriteErr(w http.ResponseWriter, err error) {
	template := `<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		</head>
		<body>
		出错了 <br />
		%+v
		</body>
	</html>`
	w.Write([]byte(fmt.Sprintf(template, err)))
}

func fenpeiBuildWarning(w http.ResponseWriter,
	bloggerResName, timmingResName, placeResName string) {
	template := `<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body> 
		<h1>生成结果link: </h1>
		<a href="https://storage.googleapis.com/ceshi_upload/%s">
			通过博主分组排列
		</a>
		<form action="/fenpei/table">
            <input type="text" id="file_url" name="file_url" value="https://storage.googleapis.com/ceshi_upload/%s" style = "display: none">
            <input type="submit" value="HTML版" />
		</form>
		<br /><br />

		<a href="https://storage.googleapis.com/ceshi_upload/%s">
			通过场地分组排列
		</a>
		<form action="/fenpei/table">
            <input type="text" id="file_url" name="file_url" value="https://storage.googleapis.com/ceshi_upload/%s" style = "display: none">
            <input type="submit" value="HTML版" />
		</form>
		<br /><br />

		<a href="https://storage.googleapis.com/ceshi_upload/%s">
			通过时间分组排列
		</a>
		<form action="/fenpei/table">
            <input type="text" id="file_url" name="file_url" value="https://storage.googleapis.com/ceshi_upload/%s" style = "display: none">
            <input type="submit" value="HTML版" />
		</form>
		<br /><br />

		<h1>以下为警告信息</h1>
		<ul>
			%s
		</ul>
	</body>
</html>`

	messages := make([]string, len(outputMsg))
	for i, msg := range outputMsg {
		messages[i] = fmt.Sprintf(`<li>%s</li>`, msg)
	}

	outputMsg = []string{}
	productMap = make(map[int][]Show, 0)

	w.Write([]byte(fmt.Sprintf(template,
		bloggerResName,
		bloggerResName,
		placeResName,
		placeResName,
		timmingResName,
		timmingResName,
		strings.Join(messages, ""))))
}

func FenpeiIndexServer(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			form {
				border: 1px solid;
			}
		</style>
	</head>
	<body>
		<div style="text-align: center">
			<h1>主播产品分配工具</h1>
			<form action="/fenpei/process" id="form1" enctype="multipart/form-data" method = "post" >
				关于主播信息的文件： <input style="font-size:30px" id="blogger" name = "blogger" type="file" /> <br /><br />

				关于产品信息的文件： <input style="font-size:30px" id="product" name = "product" type="file" /> <br /><br />
				
				关于播出时间的文件： <input style="font-size:30px" id="timing" name = "timing" type="file" /> <br /><br />
				
				每场streaming最少播产品的件数：<input id="min_1" name = "min_1" type="text" value="5" /> <br /><br />
                每场streaming最多播产品的件数：<input id="max_1" name = "max_1" type="text" value="10"/> <br /><br />
				每个产品最多被播的次数：<input id="max_2" name = "max_2" type="text" value="30"/> <br /><br />
				拿回家播的产品锁的天数：<input id="home_nb_day" name = "home_nb_day" type="text" value="2"/> <br /><br />

				<h2>页面会自动跳转，期间不要关闭页面</h1>
				<input type="submit" style='font-size:50px' value="提交" />
			</form>
		</div>
	</body>
</html>`))
}

func FenpeiTableServer(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		err = fmt.Errorf("failed storage.NewClient: %+v", err)
		fenpeiWriteErr(w, err)
		return
	}

	// 读取url信息
	fileURL := r.FormValue("file_url")

	// find file path
	index := strings.Index(fileURL, fenpeiBucket)
	filePath := fileURL[index+len(fenpeiBucket)+1:]

	fenpeiBucket := client.Bucket(fenpeiBucket)
	obj := fenpeiBucket.Object(filePath)

	// read file
	reader, err := obj.NewReader(ctx)
	if err != nil {
		err = fmt.Errorf("failed obj.NewReader: %+v", err)
		fenpeiWriteErr(w, err)
		return
	}
	defer reader.Close()

	csvReader := csv.NewReader(reader)

	resHTML := ""
	for {
		// Read each record from csv
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			err = fmt.Errorf("failed csvReader.Read: %+v", err)
			fenpeiWriteErr(w, err)
			return
		}

		resHTML = resHTML + buildTableHTML(record, len(resHTML) == 0)
	}
	resHTML = resHTML + closeTableHTML()

	resHTML = `<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			table, th, td {
				border: 1px solid black;
				border-collapse: collapse;
			}
		</style>
		</head>
		<body>` + resHTML +
		`</body>
	</html>`
	w.Write([]byte(resHTML))
}

func FenpeiServer(w http.ResponseWriter, r *http.Request) {
	minProductsPerStreaming = 5
	maxProductsPerStreaming = 10
	maxStreamingPerProduct = 30
	homeBlockTime = 2

	// Open the file
	bloggerFile, err := os.Open("./data/blogger_info.csv")
	if err != nil {
		log.Printf("failed os.Open(): %+v", err)
		return
	}

	productFile, err := os.Open("./data/products_info.csv")
	if err != nil {
		log.Printf("failed os.Open(): %+v", err)
		return
	}

	timingFile, err := os.Open("./data/timing.csv")
	if err != nil {
		log.Printf("failed os.Open(): %+v", err)
		return
	}

	/*
		// 读取conf信息
		minStr := r.FormValue("min_1")
		maxStr := r.FormValue("max_1")
		max2Str := r.FormValue("max_2")
		nbHomeDayStr := r.FormValue("home_nb_day")

		var err error
		minProductsPerStreaming, err = strconv.Atoi(minStr)
		if err != nil {
			err = fmt.Errorf("failed strconv.Atoi(%s) minProductsPerStreaming", minStr)
			fenpeiWriteErr(w, err)
			return
		}

		maxProductsPerStreaming, err = strconv.Atoi(maxStr)
		if err != nil {
			err = fmt.Errorf("failed strconv.Atoi(%s) maxProductsPerStreaming", minStr)
			fenpeiWriteErr(w, err)
			return
		}

		maxStreamingPerProduct, err = strconv.Atoi(max2Str)
		if err != nil {
			err = fmt.Errorf("failed strconv.Atoi(%s) maxStreamingPerProduct", max2Str)
			fenpeiWriteErr(w, err)
			return
		}

		homeBlockTime, err = strconv.Atoi(nbHomeDayStr)
		if err != nil {
			err = fmt.Errorf("failed strconv.Atoi(%s) homeBlockTime", nbHomeDayStr)
			fenpeiWriteErr(w, err)
			return
		}

		// 读取excel文件
		bloggerFile, _, err := r.FormFile("blogger")
		switch {
		case err == http.ErrMissingFile:
			err = fmt.Errorf("blogger file is not found")
			fenpeiWriteErr(w, err)
			return
		case err != nil:
			err = fmt.Errorf("blogger file read failed: %+v", err)
			fenpeiWriteErr(w, err)
			return
		}

		productFile, _, err := r.FormFile("product")
		switch {
		case err == http.ErrMissingFile:
			err = fmt.Errorf("product file is not found")
			fenpeiWriteErr(w, err)
			return
		case err != nil:
			err = fmt.Errorf("product file read failed: %+v", err)
			fenpeiWriteErr(w, err)
			return
		}

		timingFile, _, err := r.FormFile("timing")
		switch {
		case err == http.ErrMissingFile:
			err = fmt.Errorf("timing file is not found")
			fenpeiWriteErr(w, err)
			return
		case err != nil:
			err = fmt.Errorf("timing file read failed: %+v", err)
			fenpeiWriteErr(w, err)
			return
		}
	*/

	outputMsg = []string{}

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		err = fmt.Errorf("failed storage.NewClient: %+v", err)
		fenpeiWriteErr(w, err)
		return
	}

	// 把博主信息读出来
	bloggers, err := readBloggers(bloggerFile)
	if err != nil {
		err = fmt.Errorf("failed readBloggers(): %+v", err)
		fenpeiWriteErr(w, err)
		return
	}

	// 把产品信息读出来
	products, err := readProducts(productFile)
	if err != nil {
		err = fmt.Errorf("failed readProducts(): %+v", err)
		fenpeiWriteErr(w, err)
		return
	}
	productMap = make(map[int][]Show, len(products))

	// 把重复的产品信息去掉（按ID确定）
	/*
		products := make([]Product, 0, len(orgProducts))
		productMap = make(map[string][]Show, len(products))
		for i, p := range orgProducts {
			_, existed := productMap[p.ID]
			if existed {
				newMsg := fmt.Sprintf("this product has a duplicate: product id %s name %s link %s",
					p.ID,
					p.Name,
					p.Link)
				outputMsg = append(outputMsg, newMsg)
				continue
			}

			products = append(products, orgProducts[i])
			productMap[p.ID] = make([]Show, 0, maxStreamingPerProduct)
		}
	*/

	// 把场地和播出时间信息读出来
	shows, err := readTiming(timingFile)
	if err != nil {
		err = fmt.Errorf("failed readTiming(): %+v", err)
		fenpeiWriteErr(w, err)
		return
	}

	// 把场地和播出时间信息分配给对应的主播
	for _, show := range shows {
		addTimingToBlogger(show, bloggers)
	}

	// 将每个产品分配给主播
	// 每个产品播N次就要循环N次，通过maxStreamingPerProduct控制
	for i := 0; i < maxStreamingPerProduct; i++ {
		if i%2 == 0 {
			for _, product := range products {
				matchProductionToBlogger(product, bloggers)
			}
		} else {
			for j := len(products) - 1; j >= 0; j-- {
				matchProductionToBlogger(products[j], bloggers)
			}
		}
	}
	// 检查是否每个产品被至少播一次
	for _, p := range products {
		_, ok := productMap[p.InternalID]
		if !ok {
			newMsg := fmt.Sprintf("this product does not match any blogger: product id %s name %s link %s",
				p.ID,
				p.Name,
				p.Link)
			outputMsg = append(outputMsg, newMsg)
		}
	}

	// 检查是否每个直播场次都有足够的产品被安排
	for _, b := range bloggers {
		for _, s := range b.Shows {
			if len(s.Products) < minProductsPerStreaming {
				newMsg := fmt.Sprintf("this blogger has a invalid streaming -> too few product: blogger name %s place %s date %s time %d-%d, %d products",
					b.Name,
					s.Place,
					s.Date.Format(fenpeiDatefFormat),
					s.Start,
					s.End,
					len(s.Products))
				outputMsg = append(outputMsg, newMsg)
			}
		}
	}

	// for _, b := range bloggers {
	// 	for _, s := range b.Shows {
	// 		for _, p := range s.Products {
	// 			fmt.Printf("b %s %+v: %+v\n\n", b.Name, s, p)
	// 		}
	// 	}
	// }

	// 按主播分组，输出
	bloggerResName, err := writeOutputByBlogger(ctx, client, bloggers)
	if err != nil {
		err = fmt.Errorf("failed writeOutputByBlogger(): %+v", err)
		fenpeiWriteErr(w, err)
		return
	}

	// 按时间分组，输出
	timmingResName, err := writeOutputByTiming(ctx, client, bloggers)
	if err != nil {
		err = fmt.Errorf("failed writeOutputByTiming(): %+v", err)
		fenpeiWriteErr(w, err)
		return
	}

	// 按场地分组，输出
	placeResName, err := writeOutputByPlace(ctx, client, bloggers)
	if err != nil {
		err = fmt.Errorf("failed writeOutputByPlace(): %+v", err)
		fenpeiWriteErr(w, err)
		return
	}

	fenpeiBuildWarning(w, bloggerResName, timmingResName, placeResName)
}

type Blogger struct {
	ID   string
	Name string

	Source      string // IG, FB, TW etc.
	Nickname    string // the nickname in the source
	Link        string // link of source
	Sex         string // man, woman
	NumFans     int    // the number of fans
	AccountName string // the acount name in the source

	RecipientName string
	Address       string
	Zip           string // 75001
	City          string
	Telephone     string

	Domains []string // Sport, Beauty etc.
	Shows   []Show
}

type Product struct {
	InternalID int
	ID         string
	Name       string
	Link       string
	Domains    []string
	Status     string
}

type Show struct {
	BloggerName string
	RoomID      string
	Place       string

	Products []Product

	StartEnd
}

type StartEnd struct {
	Date  time.Time
	Start int
	End   int
}

func (se StartEnd) String() string {
	return fmt.Sprintf("%s-%d-%d", se.Date, se.Start, se.End)
}

func (s Show) IsOccupied() bool {
	return len(s.Products) >= maxProductsPerStreaming
}

func readTiming(file multipart.File) ([]Show, error) {
	r := csv.NewReader(file)

	var shows []Show
	// 第一行是名字，真正的值从第二行开始
	isFirstLine := true
	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, "failed Read()")
		}

		if isFirstLine {
			isFirstLine = false
			continue
		}

		newShow, err := newShow(record)
		if err != nil {
			continue
		}

		if skipShow(newShow) {
			newMsg := fmt.Sprintf("the timing at %d line is not valid, skip the import data", len(shows)+1)
			outputMsg = append(outputMsg, newMsg)

			continue
		}

		shows = append(shows, newShow)
	}

	return shows, nil
}

// skipShow returns true if the given show is not a valid one.
func skipShow(s Show) bool {
	// required fileds are empty -> skip
	if len(s.BloggerName) == 0 ||
		s.Date.IsZero() ||
		s.Start == 0 ||
		s.End == 0 {
		return true
	}

	return false
}

// newShow reads records and converts them into shouw object.
func newShow(record []string) (Show, error) {
	var newShow Show
	var err error

	newShow.Place = formatValue(record[TIMING_INDEX_PLACE])
	newShow.RoomID = formatValue(record[TIMING_INDEX_ROOM_ID])
	newShow.BloggerName = formatValue(record[TIMING_INDEX_BLOGGER_NAME])

	dateStr := formatValue(record[TIMING_INDEX_DATE])
	newShow.Date, err = time.Parse(fenpeiDatefFormat, dateStr)
	if err != nil {
		newMsg := fmt.Sprintf("bad value in timing file: date '%s' is not valid, blogger name %s place %s room ID %s: %+v",
			dateStr,
			newShow.BloggerName,
			newShow.Place,
			newShow.RoomID,
			err)
		outputMsg = append(outputMsg, newMsg)

		return newShow, errors.Wrapf(err, "failed time.Parse(%s)", dateStr)
	}

	startStr := formatValue(record[TIMING_INDEX_START])
	// ex: "19:00" -> 1900
	newShow.Start, err = strconv.Atoi(strings.Replace(startStr, ":", "", 1))
	if err != nil {
		newMsg := fmt.Sprintf("bad value in timing file: start time '%s' is not valid, blogger name %s place %s room ID %s",
			startStr,
			newShow.BloggerName,
			newShow.Place,
			newShow.RoomID)
		outputMsg = append(outputMsg, newMsg)

		return newShow, errors.Wrapf(err, "failed strconv.Atoi(%s) startStr", startStr)
	}

	endStr := formatValue(record[TIMING_INDEX_END])
	// ex: "20:00" -> 2000
	newShow.End, err = strconv.Atoi(strings.Replace(endStr, ":", "", 1))
	if err != nil {
		newMsg := fmt.Sprintf("bad value in timing file: end time '%s' is not valid, blogger name %s place %s room ID %s",
			endStr,
			newShow.BloggerName,
			newShow.Place,
			newShow.RoomID)
		outputMsg = append(outputMsg, newMsg)

		return newShow, errors.Wrapf(err, "failed strconv.Atoi(%s) endStr", endStr)
	}

	return newShow, nil
}

func readProducts(file multipart.File) ([]Product, error) {
	r := csv.NewReader(file)

	nb := 1
	var products []Product
	// 第一行是名字，真正的值从第二行开始
	isFirstLine := true
	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, "failed Read()")
		}

		if isFirstLine {
			isFirstLine = false
			continue
		}

		newProduct := newProduct(record, nb)
		nb++

		if skipProduct(newProduct) {
			newMsg := fmt.Sprintf("the product at %d line is not valid, skip the import data", nb)
			outputMsg = append(outputMsg, newMsg)

			continue
		}

		products = append(products, newProduct)
	}

	return products, nil
}

// newProduct reads records and converts them into product object.
func newProduct(record []string, id int) Product {
	var newProduct Product
	newProduct.InternalID = id
	newProduct.ID = formatValue(record[PRODUCTS_INFO_INDEX_ID])
	newProduct.Name = formatValue(record[PRODUCTS_INFO_INDEX_NAME])
	newProduct.Link = formatValue(record[PRODUCTS_INFO_INDEX_LINK])
	newProduct.Status = formatValue(record[PRODUCTS_INFO_INDEX_STATUS])
	newProduct.Domains = formatValues(record[PRODUCTS_INFO_INDEX_DOMAINS])

	return newProduct
}

// skipProduct returns true if the given product is not a valid one.
func skipProduct(p Product) bool {
	// required fileds are empty -> skip
	if len(p.ID) == 0 &&
		len(p.Name) == 0 &&
		len(p.Link) == 0 {
		return true
	}

	// 没收到货的跳过
	if !strings.Contains(strings.ToLower(p.Status), "ok") {
		return true
	}

	return false
}

func readBloggers(file multipart.File) ([]*Blogger, error) {
	r := csv.NewReader(file)

	var bloggers []*Blogger

	// 第一行是名字，真正的值从第二行开始
	isFirstLine := true
	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, "failed Read()")
		}

		if isFirstLine {
			isFirstLine = false
			continue
		}

		newBlogger, err := newBlogger(record)
		if err != nil {
			continue
		}

		bloggers = append(bloggers, &newBlogger)
	}

	return bloggers, nil
}

// newBlogger reads records and converts them into blogger object.
func newBlogger(record []string) (Blogger, error) {
	var err error
	var newBlogger Blogger
	newBlogger.Name = formatValue(record[BLOGGER_INFO_INDEX_NAME])
	newBlogger.Source = formatValue(record[BLOGGER_INFO_INDEX_SOURCE])
	newBlogger.Nickname = formatValue(record[BLOGGER_INFO_INDEX_NICKNAME])
	newBlogger.Link = formatValue(record[BLOGGER_INFO_INDEX_LINK])
	newBlogger.Sex = formatValue(record[BLOGGER_INFO_INDEX_SEX])

	numFansStr := formatValue(record[BLOGGER_INFO_INDEX_NUM_FANS])
	if len(numFansStr) == 0 {
		numFansStr = "0"
	}
	newBlogger.NumFans, err = strconv.Atoi(numFansStr)
	if err != nil {
		newMsg := fmt.Sprintf("this blogger %s has bad number fans '%s'",
			newBlogger.Name,
			numFansStr)
		outputMsg = append(outputMsg, newMsg)

		return newBlogger, errors.Wrapf(err, "failed strconv.Atoi(%s) numFansStr", numFansStr)
	}

	newBlogger.AccountName = formatValue(record[BLOGGER_INFO_INDEX_ACCOUNT_NAME])
	newBlogger.RecipientName = formatValue(record[BLOGGER_INFO_INDEX_RECIPIENT])
	newBlogger.Address = formatValue(record[BLOGGER_INFO_INDEX_ADDR])
	newBlogger.Zip = formatValue(record[BLOGGER_INFO_INDEX_ZIP])
	newBlogger.City = formatValue(record[BLOGGER_INFO_INDEX_CITY])
	newBlogger.Telephone = formatValue(record[BLOGGER_INFO_INDEX_TELE])
	newBlogger.Domains = formatValues(record[BLOGGER_INFO_INDEX_DOMAINS])

	return newBlogger, nil
}

func formatValue(val string) string {
	// 去掉前后的空格
	return strings.TrimSpace(val)
}

func formatValues(val string) []string {
	allDomains := formatValue(val)
	// 变小写, 这样Sport和sport都对应同一个值
	allDomains = strings.ToLower(allDomains)

	domains := strings.Split(allDomains, ",")
	// 每一个品类的值都去掉前后的空格
	for i, domain := range domains {
		domains[i] = formatValue(domain)
	}

	return domains
}

func addTimingToBlogger(show Show, bloggers []*Blogger) {
	for i, blogger := range bloggers {
		if show.BloggerName != blogger.Name {
			continue
		}

		bloggers[i].Shows = append(bloggers[i].Shows, show)
		return
	}

	// not find any matched blogger
	newMsg := fmt.Sprintf("this timing does not match any blogger: blogger name %s place %s date %d start %d end %d",
		show.BloggerName,
		show.Place,
		show.Date,
		show.Start,
		show.End)
	outputMsg = append(outputMsg, newMsg)
}

func matchProductionToBlogger(product Product, bloggers []*Blogger) {
	min := 0
	max := len(bloggers)
	startInd := rand.Intn(max-min) + min

	order := make([]int, 0, max)
	for i := startInd; i < max; i++ {
		order = append(order, i)
	}
	for i := 0; i < startInd; i++ {
		order = append(order, i)
	}

	for _, i := range order {
		blogger := bloggers[i]
		if !havaSameDomains(product.Domains, blogger.Domains) {
			continue
		}

		index := matchTiming(product.ID, blogger.Shows, productMap[product.InternalID])
		if index < 0 {
			continue
		}

		// 找到了该主播对于该产品的一个可用场次
		// 将该产品加入到该主播的单子里
		blogger.Shows[index].Products =
			append(blogger.Shows[index].Products, product)

		// 将该产品所有已匹配的map更新
		matched := productMap[product.InternalID]
		matched = append(matched, blogger.Shows[index])
		productMap[product.InternalID] = matched
		return
	}
}

func havaSameDomains(domainsProduct, domainsBlogger []string) bool {
	for _, a := range domainsProduct {
		for _, b := range domainsBlogger {
			if a == b {
				return true
			}
		}
	}

	return false
}

func isNotAcceptable(matchedTiming, freeTiming Show) bool {
	// 不在家播 或者 同一个主播 -> 只要保证同一个产品播出时间不重合就好
	if matchedTiming.Place != PlaceHome ||
		matchedTiming.BloggerName == freeTiming.BloggerName {
		return haveCrossingTiming(matchedTiming.StartEnd, freeTiming.StartEnd)
	}

	// 在家播 而且不是同一个主播

	// 产品分配给某主播在D这一天播出，homeBlockTime为2
	// 那么 D，D+1 两个整天
	// 该产品都不能分配给其他人
	for i := 0; i < homeBlockTime; i++ {
		if matchedTiming.Date.Add(time.Duration(i*24)*time.Hour).Format(fenpeiDatefFormat) == freeTiming.Date.Format(fenpeiDatefFormat) {
			return true
		}
	}

	return false
}

func haveCrossingTiming(matchedTiming, freeTiming StartEnd) bool {
	// not same day
	if matchedTiming.Date != freeTiming.Date {
		return false
	}

	// same day
	switch {
	case matchedTiming.Start < freeTiming.Start:
		if matchedTiming.End > freeTiming.Start {
			return true
		}
	case matchedTiming.Start == freeTiming.Start:
		return true
	case matchedTiming.Start > freeTiming.Start:
		if freeTiming.End > matchedTiming.Start {
			return true
		}
	}

	return false
}

// matchTiming 检查该主播对于该产品是否有可用的时间段
// 1. 该主播还有剩余的空的场次
// 2. 同一个场次还没有安排该产品
// 3. 同一个时间段，其他主播不能播同样的产品
// 4. 同一场直播不能播同样产品（产品ID一样）
func matchTiming(pID string, allShows []Show, matchedTiming []Show) int {
	for index, show := range allShows {
		// 同样的产品ID
		sameID := false
		for _, matchedProduct := range show.Products {
			if matchedProduct.ID == pID {
				sameID = true
				break
			}
		}
		if sameID {
			continue
		}

		// 本场show的products是否已满
		if show.IsOccupied() {
			continue
		}

		// 该产品还没有被安排给任何其他主播
		if len(matchedTiming) == 0 {
			return index
		}

		// 该产品在同一时间段只能播一次
		crossing := false
		for _, timing := range matchedTiming {
			if isNotAcceptable(timing, show) {
				crossing = true
				break
			}
		}

		if crossing {
			continue
		}

		return index
	}

	return -1
}

func writeOutputByPlace(ctx context.Context, client *storage.Client, bloggers []*Blogger) (string, error) {
	bkt := client.Bucket(fenpeiBucket)

	name := outputName(OUTPUT_FILE_PLACE_PATH)
	obj := bkt.Object(name)
	wr := obj.NewWriter(ctx)

	writer := csv.NewWriter(wr)

	// 第一行信息
	data := []string{
		`场地`,
		`日期`,
		`开始时间`,
		`结束时间`,
		`博主`,
		`产品ID`,
		`产品名字`,
		`产品链接`,
	}

	err := writer.Write(data)
	if err != nil {
		return name, errors.Wrap(err, "failed writer.Write()")
	}

	allPlace := make(map[string]bool)
	for _, blogger := range bloggers {
		for _, show := range blogger.Shows {
			_, existed := allPlace[show.Place]
			if !existed {
				allPlace[show.Place] = true
			}
		}
	}

	for place, _ := range allPlace {
		nb := 0
		for _, blogger := range bloggers {
			for _, show := range blogger.Shows {
				if show.Place != place {
					continue
				}

				if len(show.Products) == 0 {
					data = []string{
						show.Place,
						show.Date.Format(fenpeiDatefFormat),
						fmt.Sprintf("%d", show.Start),
						fmt.Sprintf("%d", show.End),
						blogger.Name,
						"",
						"",
						"",
					}

					err = writer.Write(data)
					if err != nil {
						return name, errors.Wrap(err, "failed writer.Write()")
					}
				}

				sameShowNb := 0
				for _, p := range show.Products {
					var placeVal, dateVal, startVal, endVal string
					switch {
					case nb == 0:
						placeVal = show.Place
						dateVal = show.Date.Format(fenpeiDatefFormat)
						startVal = fmt.Sprintf("%d", show.Start)
						endVal = fmt.Sprintf("%d", show.End)
					case sameShowNb == 0:
						dateVal = show.Date.Format(fenpeiDatefFormat)
						startVal = fmt.Sprintf("%d", show.Start)
						endVal = fmt.Sprintf("%d", show.End)
					default:
						// all empty
					}

					sameShowNb++
					nb++

					data = []string{
						placeVal,
						dateVal,
						startVal,
						endVal,
						blogger.Name,
						p.ID,
						p.Name,
						p.Link,
					}

					err = writer.Write(data)
					if err != nil {
						return name, errors.Wrap(err, "failed writer.Write()")
					}
				}
			}
		}
	}

	writer.Flush()

	err = wr.Close()
	if err != nil {
		return name, errors.Wrap(err, "failed wr.Close()")
	}

	return name, nil
}

func writeOutputByTiming(ctx context.Context, client *storage.Client, bloggers []*Blogger) (string, error) {
	bkt := client.Bucket(fenpeiBucket)
	name := outputName(OUTPUT_FILE_TIMING_PATH)
	obj := bkt.Object(name)
	wr := obj.NewWriter(ctx)

	writer := csv.NewWriter(wr)

	// 第一行信息
	data := []string{
		`日期`,
		`开始时间`,
		`结束时间`,
		`博主`,
		`产品ID`,
		`产品名字`,
		`产品链接`,
	}

	err := writer.Write(data)
	if err != nil {
		return name, errors.Wrap(err, "failed writer.Write()")
	}

	allTiming := make(map[string]bool)
	for _, blogger := range bloggers {
		for _, show := range blogger.Shows {
			_, existed := allTiming[show.StartEnd.String()]
			if !existed {
				allTiming[show.StartEnd.String()] = true
			}
		}
	}

	for seStr, _ := range allTiming {
		nb := 0
		for _, blogger := range bloggers {
			for _, show := range blogger.Shows {
				if show.StartEnd.String() != seStr {
					continue
				}

				if len(show.Products) == 0 {
					data = []string{
						show.Date.Format(fenpeiDatefFormat),
						fmt.Sprintf("%d", show.Start),
						fmt.Sprintf("%d", show.End),
						blogger.Name,
						"",
						"",
						"",
					}

					err = writer.Write(data)
					if err != nil {
						return name, errors.Wrap(err, "failed writer.Write()")
					}
				}

				for _, p := range show.Products {
					if nb == 0 {
						data = []string{
							show.Date.Format(fenpeiDatefFormat),
							fmt.Sprintf("%d", show.Start),
							fmt.Sprintf("%d", show.End),
							blogger.Name,
							p.ID,
							p.Name,
							p.Link,
						}

						nb++
					} else {
						data = []string{
							"",
							"",
							"",
							blogger.Name,
							p.ID,
							p.Name,
							p.Link,
						}
					}

					err = writer.Write(data)
					if err != nil {
						return name, errors.Wrap(err, "failed writer.Write()")
					}
				}
			}
		}
	}

	writer.Flush()
	err = wr.Close()
	if err != nil {
		return name, errors.Wrap(err, "failed wr.Close()")
	}

	return name, nil
}

func writeOutputByBlogger(ctx context.Context, client *storage.Client, bloggers []*Blogger) (string, error) {
	bkt := client.Bucket(fenpeiBucket)
	name := outputName(OUTPUT_FILE_BLOGGER_PATH)
	obj := bkt.Object(name)
	wr := obj.NewWriter(ctx)

	writer := csv.NewWriter(wr)

	// 第一行信息
	data := []string{
		`博主`,
		`日期`,
		`开始时间`,
		`结束时间`,
		`产品ID`,
		`产品名字`,
		`产品链接`,
	}

	err := writer.Write(data)
	if err != nil {
		return name, errors.Wrap(err, "failed writer.Write()")
	}

	for _, blogger := range bloggers {
		for _, show := range blogger.Shows {
			if len(show.Products) == 0 {
				data = []string{
					blogger.Name,
					show.Date.Format(fenpeiDatefFormat),
					fmt.Sprintf("%d", show.Start),
					fmt.Sprintf("%d", show.End),
					"",
					"",
					"",
				}

				err = writer.Write(data)
				if err != nil {
					return name, errors.Wrap(err, "failed writer.Write()")
				}
			}

			for i, p := range show.Products {
				if i == 0 {
					data = []string{
						blogger.Name,
						show.Date.Format(fenpeiDatefFormat),
						fmt.Sprintf("%d", show.Start),
						fmt.Sprintf("%d", show.End),
						p.ID,
						p.Name,
						p.Link,
					}
				} else {
					data = []string{
						"",
						"",
						"",
						"",
						p.ID,
						p.Name,
						p.Link,
					}
				}

				err = writer.Write(data)
				if err != nil {
					return name, errors.Wrap(err, "failed writer.Write()")
				}
			}
		}
	}

	writer.Flush()

	err = wr.Close()
	if err != nil {
		return name, errors.Wrap(err, "failed wr.Close()")
	}

	return name, nil
}

func outputName(prefix string) string {
	return fmt.Sprintf("%s_%d.csv", prefix, time.Now().Unix())
}

func buildTableHTML(data []string, isFirst bool) string {
	var res string

	nameNode := "td"
	if isFirst {
		res = `<table>`
		nameNode = "th"
	}

	res = res + "<tr>"
	for _, val := range data {
		res = res + fmt.Sprintf("<%s>%s</%s>", nameNode, val, nameNode)
	}
	res = res + "</tr>"

	return res
}

func closeTableHTML() string {
	return "</table>"
}
